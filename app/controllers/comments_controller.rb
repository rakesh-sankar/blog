class CommentsController < ApplicationController
  before_filter :find_post
  respond_to :html, :js

  def new
    @comment = @post.comments.build

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comment }
    end
  end

  def create
    @comment = Comment.new(comment_params)
    @comment.post_id = params[:post_id]

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @comment.post, notice: 'Comment was successfully created.' }
        format.json {render action: 'show', status: :created, location: @comment}
        format.js
      else
        format.html { render action: 'new' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
        format.js
      end
    end

  end

  def comment_params
    params.require(:comment).permit(:body)
  end

  private
  def find_post
    @post = Post.find(params[:post_id])
  end
end
